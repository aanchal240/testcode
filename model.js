const mongoose = require('mongoose')

const testSchema = new mongoose.Schema({
    name: {
        type: String,
        trim: true
    },
    age: {
        type: Number,
        required: true
    },
    numbers: [
        {
            type: Number,
            required: true
        }
    ],
    colour:{
       type:mongoose.Schema.Types.ObjectId,
       ref:'colour'
    }

})

module.exports = mongoose.model('test', testSchema)