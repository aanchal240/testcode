const mongoose = require('mongoose')
const colourSchema=new mongoose.Schema({
    
    color:{
        type:String
    },
    address:{
        type:mongoose.Schema.Types.ObjectId,
        ref:'Address2'
    },
})
const colour=mongoose.model('colour',colourSchema)
module.exports=colour