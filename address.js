const mongoose = require('mongoose')
const address2Schema=new mongoose.Schema({
    
    city: 
    {
        type:String
    },
    street:
    { 
        type:String
    },
    houseNumber: 
    {
      type:Number
    }
})
const address=mongoose.model('Address2',address2Schema)
module.exports=address