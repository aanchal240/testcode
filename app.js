const express = require('express')
const port = 3000
const app = express()
const data=require('./model.js')
const address=require('./address.js')
const colour=require('./colour.js')
app.use(express.json())
const mongoose = require('mongoose');
const res = require('express/lib/response')
const async =require('async')

//using arrays
// async.series([
//     function(callback) {
//         setTimeout(function() {
//             // do some async task
//             callback(null, 'one');
//         }, 200);
//     },
//     function(callback) {
//         setTimeout(function() {
//             // then do another async task
//             callback(null, 'two');
//         }, 100);
//     }
// ], function(err, results) {
//     console.log(results);
//     // results is equal to ['one','two']
// });

//using Object

// async.series({
//     one: function(callback) {
//         setTimeout(function() {
//             // do some async task
//             callback(null, 1);
//         }, 200);
//     },
//     two: function(callback) {
//         setTimeout(function() {
//             // then do another async task
//             callback(null, 2);
//         }, 100);
//     }
// }, function(err, results) {
//     console.log(results);
//     // results is equal to: { one: 1, two: 2 }
// });

//Using Promises
// async.series([
//     function(callback) {
//         setTimeout(function() {
//             callback(null, 'one');
//         }, 200);
//     },
//     function(callback) {
//         setTimeout(function() {
//             callback(null, 'two');
//         }, 100);
//     }
// ]).then(results => {
//     console.log(results);
//     // results is equal to ['one','two']
// }).catch(err => {
//     console.log(err);
// });

//Using async/await

// const foo = async () =>   {
//     try {
//         let results = await async.series([
//             function(callback) {
//                 setTimeout(function() {
//                     // do some async task
//                     callback(null, 'one');
//                 }, 200);
//             },
//             function(callback) {
//                 setTimeout(function() {
//                     // then do another async task
//                     callback(null, 'two');
//                 }, 100);
//             }
//         ]);
//         console.log(results);
//         // results is equal to ['one','two']
//     }
//     catch (err) {
//         console.log(err);
//     }
// }
// foo()

// async.waterfall([
//     function(callback)
//     {
//         callback(null,2,3)
//     },
//     function (a,b,callback)
//     {
//         callback(null,a*b)
//     },
// ],
// function(err,result)
//     {
//         if(err)
//         return console.log(err)
//         console.log(result)
//     })

//Using names functions
async.waterfall([
    fun1,
    fun2,
    fun3
], function (err, result) {
    if (err) return console.log(err)
    console.log(result)
})


function fun1(callback) {
    callback(null, 1, 2)
}

function fun2(a, b, callback) {
    callback(null, a + b)
}

function fun3(c, callback) {
    callback(null, c + 4)
}

mongoose.connect('mongodb://127.0.0.1:27017/testCode')


// app.get('/',async(req,res)=>{
//     try{
    
//        const output=await data.aggregate([
//               {$lookup: {
//                 from: 'colours', // collection 2 name
//                 localField: 'colour', //field name is 1st table
//                 foreignField: '_id', //Primary key of second table
//                 "pipeline": [
//                     { "$project": { "_id": 0, "__v": 0} }
//                 ],
//                 as: 'lookup',
                
//             }
        
        
//         },
//         {
//             $unwind: {
//                 path: "$lookup",
//                 preserveNullAndEmptyArrays: true
//             }
//         },
//         {
//             $lookup: {
//                 from: 'address2', // collection 2 name
//                 localField: 'lookup.address', //field name is 1st table
//                 foreignField: '_id', //Primary key of second table
//                 "pipeline": [
//                     { "$project": { "_id": 0, "__v": 0} }
//                 ],
//                 as: 'details',
//             },
//         },
//            {$project:{_id:0,__v:0}},
//        ])
     
//      res.send(output)
   
// }
//     catch(err)
//     {
//         console.log(err.message)
//     }
   
// })

// run()
// async function run()
// {
//     try{
//        const output = await colour.create({
//         color:'blue',
//         address:'61fd1044b85c13338728ed42'
//        })
//        await output.save()
//        console.log(output)
//     }
//     catch(err)
//     {
//         console.log(err.message)
//     }
//  }

// run()
// async function run()
// {
//     try{
//        const output=await data.create({
//            name:'c',
//            age:22,
//            numbers:[1,2,3],
//            colour:'61fd26fc3ec03ece308693ac'
//        })
//        await output.save()
//        console.log(output)
//        //res.send(output)
//     }
//     catch(err)
//     {
//         console.log(err.message)
//     }
// }

// run()
// async function run()
// {
//     try{
//        const output = await address.create({
//         city:'khanna',
//         street:'narotam nagar',
//         houseNumber:240
//        })
//        await output.save()
//        console.log(output)
//     }
//     catch(err)
//     {
//         console.log(err.message)
//     }
// }

app.listen(port, (error) => {
    if (error) {
        return console.log('Error starting server \n' + error)
    }
    console.log('Server is up and running on port: ' + port)
})